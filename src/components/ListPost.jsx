import React, { useState , useEffect} from "react";
import Post from "./Post";
import axios from 'axios' ; 
const ListPost =()=>{
    const [data,setData]=useState([])
    useEffect(()=>{
        axios("")
            .then(resp => setData(resp.data))
            .catch(err => console.log(err)) ;
    },[]) ; 
    return (
        <>
            {
                data.map(post => <div>{post}</div>)
            }
        </>
          )
}