import React from 'react'
import {Link } from 'react-scroll'
const Nav = () => {
  return (
      <header>
        <div class="top-bar d-none d-md-block">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-xl-6 offset-xl-1 col-lg-6 offset-lg-0 col-md-7 offset-md-1">
                        <div class="header-info">
                            <span><i class="fas fa-phone"></i> +216 22 360 607</span>
                            <span><i class="fas fa-envelope"></i> maylina@yahoo.com</span>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-4">
                        <div class="header-top-right-btn f-right">
                            <a href="appoinment.html" class="btn">Make Appointment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-5 d-flex align-items-center">
                        <div class="logo logo-circle pos-rel">
                            <a href="index.html"><img src="img/logo/logo.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9">
                        <div class="header-right f-right">
                            <div class="header-lang f-right pos-rel d-none d-lg-block">
                                <div class="lang-icon">
                                    <span>EN<i class="fas fa-angle-down"></i></span>
                                </div>
                              
                            </div>
                       
                        </div>
                        <div class="header__menu f-right">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><Link href="index.html" 
                                to='HIV' spy={true} smooth={true} offset={50} duration={500}> Home +</Link>
                                     
                                    </li>
                                    <li><Link to='about-us' spy={true} smooth={true} offset={50} duration={500} href="services.html">Department +</Link>
                                     
                                    </li>
                                    <li><Link to='latest' spy={true} smooth={true} offset={50} duration={500}  href="doctor.html">Doctors +</Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </header>
  )
}

export default Nav