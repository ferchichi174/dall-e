import React from 'react'

const Footer = () => {
    return (
        <footer>
            <div class="footer-top primary-bg footer-map pos-rel pt-120 pb-80">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="footer-contact-info footer-contact-info-3 mb-40">
                                <div class="footer-logo mb-35">
                                    <a href="#"><img src="img/logo/footer-logo-3.png" alt="" /></a>
                                </div>
                                <div class="footer-contact-content mb-25">

                                </div>
                                <div class="footer-emailing">
                                    <ul>
                                        <li><i class="far fa-clone"></i>linamay@yahoo.com</li>
                                        <li><i class="far fa-flag"></i>Tunisia sousse</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="footer-widget mb-40">
                                <div class="footer-title">
                                    <h3>Departments</h3>
                                </div>
                                <div class="footer-menu footer-menu-2">
                                    <ul>
                                        <li><a href="#">Surgery and Radiology</a></li>
                                        <li><a href="#">Departments</a></li>
                                        <li><a href="#">Family Medicine</a></li>
                                        <li><a href="#">Our Doctors</a></li>
                                        <li><a href="#">Women’s Health</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Optician</a></li>
                                        <li><a href="#">Shop</a></li>
                                        <li><a href="#">Pediatrics</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                        <li><a href="#">Dermatology</a></li>
                                        <li><a href="#">Book an Appointment</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            <div class="footer-bottom pt-25 pb-20">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="footer-copyright footer-copyright-3 text-center">
                                <p>Copyright by@ BasicTheme - 2022</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer