import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <main>
    
    <section class="about-area pt-120 pb-90"  id="HIV">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-5">
                    <div class="about-left-side pos-rel mb-30">
                        <div class="about-front-img pos-rel mt-[10px]">
                            <img src="https://www.essentiel-sante-magazine.fr/wp-content/uploads/2018/05/idees-recues-sur-le-sida-498x281.jpg" alt="" className='w-[100%] h-[300px]'/>
                            
                        </div>
                    
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7">
                    <div class="about-right-side pt-55 mb-30">
                        <div class="about-title mb-20">
                            <h1>Sida c’est quoi ? </h1>
                        </div>
                        <div class="about-text mb-50">
                            <p>
Le Virus de l’Immunodéficience Humaine provoque une infection virale qui attaque le système immunitaire et l'empêche de fonctionner correctement. En effet,le virus affaiblit le système immunitaire le rendant vulnérable à de multiples infections opportunistes. Ainsi, le VIH/SIDA reste une maladie mortelle si elle n’est pas traitée.
</p>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="appoinment-section pt-120 pb-120" data-background="img/bg/appointment.jpg" id="about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="appoinment-box white">
                        <div class="appoinment-content">
                            <span class="small-text"></span>
                            <h1>Transmission</h1>
                            <p>Le VIH se transmet par pénétration (anale ou vaginale) lors d'un rapport sexuel, par transfusion sanguine, par le partage d'aiguilles contaminées dans les établissements de soin et chez les toxicomanes, mais aussi de la mère à l'enfant au cours de la grossesse, de l'accouchement et de l'allaitement.</p>
                            <hr />
                            <p>Le VIH se transmet par pénétration (anale ou vaginale) lors d'un rapport sexuel, par transfusion sanguine, par le partage d'aiguilles contaminées dans les établissements de soin et chez les toxicomanes, mais aussi de la mère à l'enfant au cours de la grossesse, de l'accouchement et de l'allaitement.</p>
                           
                        </div>
                        <a href="#" class="btn mt-40">make appointment</a>
                    </div>
                </div>
                <div>
                <img src="https://www.enipse.fr/wp-content/uploads/2020/06/vih-800x445.jpg" alt="" />
                </div>
            </div>
        </div>
    </section>
      <section class="team-area pt-115 pb-55">
        <h1>Links</h1>
          <div>
            <Link to="/advice">
            <h1>Advice</h1>
            <hr />
            <p></p>
            </Link>
          </div>
          <div>
          <Link to="/image">
          <h1>image</h1>
          <hr />
          <p></p>
          </Link>
          </div>
          <div>

          <Link to="/your_mood">
          <h1>your mood</h1>
          <hr />
          <p></p>
          </Link>

          </div>
          <div></div>
      </section>
      
   
    <section class="analysis-area pos-rel theme-bg pb-90" id="latest">
        <div class="analysis-bg-icon">
            <img src="img/analysis/analysis-bg-icon.png" alt=""/>
        </div>
        <div class="container">
            
            <div class="row">
                <div class="col-xl-12">
                    <div class="tab-content" id="pills-tabContent">
                        
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                          
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="row">
                                <div class="col-xl-6 col-lg-8">
                                    <div class="section-title pos-rel mb-40">
                                        <div class="section-text section-text-white section-text-green pos-rel">
                                            <h5>Improve your experience.</h5>
                                            <h1 class="white-color">Professionals We Care About You.</h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                                labore
                                                et dolore magna
                                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                                ex ea
                                                commodo consequat.
                                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                                pariatur.</p>
                                        </div>
                                    </div>
                                    <div class="section-button section-button-left mb-30">
                                        <a data-animation="fadeInLeft" data-delay=".6s" href="#"
                                            class="btn btn-icon ml-0"><span>+</span>Make
                                            Appointment</a>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-4">
                                    <div class="analysis-chart mb-30">
                                        <img src="img/analysis/chart.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

  )
}

export default Home