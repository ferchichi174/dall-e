import React, { useEffect, useState } from 'react'
import axios from "axios";
import Loading from '../components/Loading';
const GetAdvice = () => {
  const [advice, setAdvice] = useState([]);
  const [Loading, setLoading] = useState(true)
  // const [value , setValue] = useState("") ; 
  // const handleSubmit = async(e)=>{
  //     e.preventDefault();
  //    const result=  await axios.post("http://192.168.137.98/advice:5000",{story: value}) ; 
  //    setAdvice(result.data) ; 
  //    console.log("result ====>",result.data) 
  //     // content  
  //   }
  useEffect(() => {
    axios("http://192.168.137.98:5000/advice")
      .then(resp => {
        console.log(Object.values(resp.data));
        setAdvice(Object.values(resp.data)); setLoading(false)
      })
      .catch(err => console.log(err))

  }, [])
  if (Loading) return <h1>Loading..</h1>
  return (
    <div className='one'>
      {/*     Texxt    */}

      {advice.length > 0 && advice.map((singleAdvice , index) =>
        <p key={index}>{singleAdvice}</p>
      )}
    </div>
  )
}

export default GetAdvice