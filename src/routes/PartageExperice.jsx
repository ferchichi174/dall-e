import React ,{ useState , useEffect}from 'react'
import axios from 'axios' ; 

const PartageExperice = () => {
    const [ text, setText]  = useState("") ; 
    useEffect(()=>{
        if(text.length> 100)
            setText(text) ; 
            

    },[])
    const [data , setData] = useState("")
    const handleSubmit = async(e)=>{
        e.preventDefault();
                                                    
        // fetch("http://192.168.137.98:5000", {
        //     method: 'POST',
        //     mode:'cors',
        //     body: JSON.stringify({
        //         story: text})
        // }).then(resp => console.log(resp.json()))

       const result=  await axios.post("http://192.168.137.98:5000",{story: text}) ; 
       setData(result.data) ; 
       console.log("result ====>",result.data) 
        // content  
      }
   
  return (
    <form className='one'>
        <textarea name="story" placeholder="text" rows="4" cols="50" onChange={e =>setText(e.target.value)}>

        </textarea>
        <button onClick={e =>handleSubmit(e)}>
            valid
        </button>
        {data.status || <p>{data?.correction?.story}</p>}
    </form>
  )
}

export default PartageExperice