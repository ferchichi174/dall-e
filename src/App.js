import './App.css';
import {BrowserRouter , Routes ,  Route } from "react-router-dom"
import Moods from './routes/Moods';
import Nav from './components/Nav';
import Footer from './components/Footer';
import Home from './routes/Home';
import GetAdvice from './routes/GetAdvice';
import GetImage from './routes/GetImage';
function App() {
  return (
    <BrowserRouter>
      <Nav/>
      <Routes>
        <Route  path='/' element={<Home/>}/>
        <Route  path='/advice' element={<GetAdvice/>}/>
        <Route  path='/image' element={<GetImage/>}/>
        <Route  path='/your_mood' element={<Moods/>}/>

      </Routes>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
