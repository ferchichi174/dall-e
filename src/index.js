import React from 'react';
import ReactDOM from 'react-dom/client';
import Nav from './components/Nav';
import './index.css'
import './css/default.css' ;  
import './css/meanmenu.css' ;  
import './css/nice-select.css' ;  
import './css/style.css' ;  
import './css/slick.css' ;  
import './css/responsive.css' ; 
import './css/owl.carousel.min.css' ; 
import './css/nice-select.css' ; 
import './css/magnific-popup.css' ; 
import './css/all.min.css' ;
import './css/animate.min.css' ; 
import './css/bootstrap.min.css'
import Footer from './components/Footer';
import Home from './routes/Home';
import App from './App';
import GetAdvice from './routes/GetAdvice';
import GetImage from './routes/GetImage';
import Moods from './routes/Moods';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App/>
);

